package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/model"
)

//StatusUpdate Structure of status update message got bs
type StatusUpdate struct {
	MsgType string `json:"MsgType"`
	Status  string `json:"Status"`
	Sid     string `json:"Sid"`
}

//CallBackHandler handler for the status call back
func CallBackHandler(c *gin.Context) {
	statusUpdate := StatusUpdate{MsgType: "StatusUpdate"}
	statusUpdate.Status = c.PostForm("Status")
	statusUpdate.Sid = c.PostForm("Sid")
	client, err := model.NewBSClient()
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
	}
	var jobID uint64
	jobID, err = client.Put(statusUpdate)
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
	}
	c.JSON(http.StatusOK, jobID)
}
