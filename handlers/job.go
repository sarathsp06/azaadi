package handlers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/model"
	"github.com/sarathsp06/azaadi/model/jobs"
)

//JobMessage is the message for bs
type JobMessage struct {
	MsgType string `json:"MsgType"`
	Job     uint64 `json:"JobId"`
}

//JobPostHandler handles the outgoing webhook from slack
//to mock the request do this
//  curl -XPOST -H "Content-Type:application/x-www-form-urlencoded"  http://127.0.0.1:8080/Job --data "token=sasasasaegxsdh&team_id=T03PTHYK9&team_domain=robohome&service_id=838312sasa25&channel_id=C07AsasaASXMH&channel_name=product&timestamp=1438203624.000004&user_id=U03PTHYLB&user_name=sarath&text=testing"
func JobPostHandler(c *gin.Context) {
	var message = &jobs.Job{}
	if err := c.BindJSON(message); err == nil {
		err = jobs.Insert(message)
		if err != nil {
			c.String(http.StatusExpectationFailed, err.Error())
		} else {
			err = indexJob(message)
			if err != nil {
				c.String(http.StatusExpectationFailed, err.Error())

			} else {
				c.JSON(http.StatusOK, *message)
			}
		}
	} else {
		c.String(http.StatusExpectationFailed, err.Error())
	}
}

func indexJob(job *jobs.Job) error {
	client, err := model.NewBSClient()
	if err != nil {
		return err
	}
	msg := &JobMessage{MsgType: "NewJob"}
	msg.Job = job.ID
	_, err = client.Put(msg)
	if err != nil {
		return err
	}
	return nil
}

//JobGetHandler handles the get requests
func JobGetHandler(c *gin.Context) {
	producer := c.Param("producer")
	producerID, err := strconv.ParseUint(producer, 0, 64)
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
		return
	}
	var allJob []jobs.Job
	allJob, err = jobs.GetByProducer(producerID)
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
		return
	}
	c.JSON(http.StatusOK, allJob)
}
