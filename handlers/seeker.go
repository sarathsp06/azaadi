package handlers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/model"
	"github.com/sarathsp06/azaadi/model/seekers"
)

//SeekerMessage is the message for bs
type SeekerMessage struct {
	MsgType string          `json:"MsgType"`
	Seeker  *seekers.Seeker `json:"Seeker"`
}

//SeekerPostHandler is the handler for SeekerPostRequests Handler
func SeekerPostHandler(c *gin.Context) {
	var message = &seekers.Seeker{}
	if err := c.BindJSON(message); err == nil {
		err = seekers.Insert(message)
		if err != nil {
			c.String(http.StatusExpectationFailed, err.Error())

		} else {
			err = indexSeeker(message)
			if err != nil {
				c.String(http.StatusExpectationFailed, err.Error())
				return
			}
			c.JSON(http.StatusOK, *message)
		}
	} else {
		c.String(http.StatusExpectationFailed, err.Error())
	}
}

func indexSeeker(seeker *seekers.Seeker) error {
	client, err := model.NewBSClient()
	if err != nil {
		return err
	}
	msg := &SeekerMessage{MsgType: "IndexSeeker"}
	msg.Seeker = seeker
	_, err = client.Put(msg)
	if err != nil {
		return err
	}
	return nil
}

//SeekerGetHandler handles the get requests
func SeekerGetHandler(c *gin.Context) {
	id := c.Param("id")
	seekerID, err := strconv.ParseUint(id, 0, 64)
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
		return
	}
	var seeker *seekers.Seeker
	seeker, err = seekers.GetByID(seekerID)
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
		return
	}
	c.JSON(http.StatusOK, seeker)
}
