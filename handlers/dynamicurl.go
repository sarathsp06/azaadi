package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/model/jobs"
	"github.com/sarathsp06/azaadi/model/mappings"
)

var responseFormat = "Wecome to HeadHeldHigh %s"

//DynamicURLHandler handler for the status call back
func DynamicURLHandler(c *gin.Context) {
	sid := c.Query("Sid")
	mapping, err := mappings.GetBySID(sid)
	if err != nil || mapping == nil || mapping.JobID == 0 {
		c.String(http.StatusExpectationFailed, err.Error())
		return
	}
	jobID := mapping.JobID
	var job *jobs.Job
	job, err = jobs.GetByID(jobID)
	if err != nil || job == nil || job.AudioDesc == "" {
		c.String(http.StatusExpectationFailed, err.Error())
		return
	}
	//try passing the url of the audio uploaded
	//we have upload handler written in /handlers/upload_recording.go
	c.String(http.StatusOK, fmt.Sprintf(responseFormat, job.Description))
	return
}
