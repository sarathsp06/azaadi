package handlers

import (
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

//RecordingUploadHandler handles the post requset to employers
func RecordingUploadHandler(c *gin.Context) {
	file, header, err := c.Request.FormFile("upload")
	filename := header.Filename
	path := filepath.Join(os.Getenv("GOPATH"), "src/github.com/sarathsp06/azaadi/assets", filename)
	out, err := os.Create(path)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	_, err = io.Copy(out, file)
	if err != nil {
		c.String(http.StatusOK, err.Error())
	}
}
