package handlers

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/model"
)

//Passthrough Structure of status update message got bs
type Passthrough struct {
	MsgType string `json:"MsgType"`
	Status  string `json:"Status"`
	Sid     string `json:"Sid"`
}

//PassthroughHandler handler for the status call back
func PassthroughHandler(c *gin.Context) {
	statusUpdate := StatusUpdate{MsgType: "RequestStatus"}
	digit := c.Query("digits")
	if strings.TrimSpace(digit) == "1" {
		statusUpdate.Status = "Accepted"
	} else {
		statusUpdate.Status = "Rejected"
	}

	statusUpdate.Sid = c.PostForm("Sid")
	client, err := model.NewBSClient()
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
	}
	var jobID uint64
	jobID, err = client.Put(statusUpdate)
	if err != nil {
		c.String(http.StatusExpectationFailed, err.Error())
	}
	c.JSON(http.StatusOK, jobID)
}
