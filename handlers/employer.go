package handlers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/model/employers"
)

//EmployerPostHandler handles the post requset to employers
func EmployerPostHandler(c *gin.Context) {
	var message = &employers.Employer{}
	if err := c.BindJSON(message); err == nil {
		err = employers.Insert(message)
		if err != nil {
			c.String(http.StatusExpectationFailed, err.Error())
		} else {
			c.JSON(http.StatusOK, *message)
		}
	} else {
		c.String(http.StatusExpectationFailed, err.Error())
	}
}

//EmployerLoginHandler handles the post requset to employers
func EmployerLoginHandler(c *gin.Context) {
	var message = &employers.Employer{}
	if err := c.BindJSON(message); err == nil {
		if message.Email == "" || message.Token == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "unauthorized"})
			return
		}
		message, err = employers.GetByCredentials(message.Email, message.Token)

		if err != nil {
			c.String(http.StatusExpectationFailed, err.Error())
		} else {
			c.JSON(http.StatusOK, *message)
		}
	} else {
		c.String(http.StatusExpectationFailed, err.Error())
	}
}

func validateRequest(c *gin.Context) bool {
	//channelName := c.PostForm("channel_name")
	//token := c.PostForm("token")
	return true
}

func timeStampFromUnix(unixt string) time.Time {
	i, err := strconv.ParseFloat(unixt, 10)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(int64(i), 0)
	return tm
}
