package seekers

import (
	"errors"
	"time"

	"github.com/sarathsp06/azaadi/model"
)

//Seeker is the job seekers details table
type Seeker struct {
	ID        uint64 `sql:"AUTO_INCREMENT"`
	Name      string
	PhotoURL  string
	UUID      string `sql:"unique_index"`
	UUIDType  string
	Dob       *time.Time
	Address   string
	Latitude  string
	Longitude string
	Number    string `sql:"unique_index"`
	Skill     string `sql:"type:text"`
	JobData   string `sql:"type:text"`
	Internal  string
	Rating    string
	CreatedAt *time.Time `sql:"DEFAULT:current_timestamp"`
	UpdatedAt *time.Time
	Status    string `sql:"type:enum('active','inactive')"`
}

//TableName returns the table name to be used for
//the User struct when communicating with db
func (t Seeker) TableName() string {
	return "seekers"
}

//Create creates contacts table in all the shards available
func Create() error {
	db, err := model.SQLConnection()
	if err != nil {
		return err
	}

	if !db.HasTable(&Seeker{}) {

		err = db.CreateTable(&Seeker{}).Error
		if err != nil {
			return err

		}
	} else {
		return errors.New("TABLEEXISTS:Seekers")
	}

	return nil
}

//Insert inserts a Seeker entry into the table
//The name is mandatory
//SeekerID is ignored even of passed
func Insert(seeker *Seeker) error {
	if seeker.Name == "" ||
		seeker.UUID == "" ||
		seeker.Number == "" ||
		seeker.Latitude == "" ||
		seeker.Longitude == "" {
		return errors.New("MANDATORY:DETAILSNOTAVAILABLE")
	}

	db, err := model.SQLConnection()
	if err != nil {
		return err
	}
	err = db.Create(seeker).Error
	if err != nil {
		return err
	}
	return nil
}

//GetByID gets a job given the id
func GetByID(ID uint64) (*Seeker, error) {
	db, err := model.SQLConnection()
	if err != nil {
		return nil, err
	}
	var seeker = &Seeker{}
	err = db.First(seeker, ID).Error
	if err != nil {
		return nil, err
	}
	return seeker, nil
}

var _ = Create()
