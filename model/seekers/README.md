###seekers
seekers table stores all job seekers information

###wSchema
| Field      | Type                      | Null | Key | Default           | Extra          |
|------------|---------------------------|------|-----|-------------------|----------------|
| id         | bigint(20) unsigned       | NO   | PRI | NULL              | auto_increment |
| name       | varchar(255)              | YES  |     | NULL              |                |
| photo_url  | varchar(255)              | YES  |     | NULL              |                |
| uuid       | varchar(255)              | YES  |     | NULL              |                |
| uuid_type  | varchar(255)              | YES  |     | NULL              |                |
| dob        | timestamp                 | YES  |     | NULL              |                |
| address    | varchar(255)              | YES  |     | NULL              |                |
| number     | varchar(255)              | YES  |     | NULL              |                |
| skill      | text                      | YES  |     | NULL              |                |
| job_data   | text                      | YES  |     | NULL              |                |
| internal   | varchar(255)              | YES  |     | NULL              |                |
| rating     | varchar(255)              | YES  |     | NULL              |                |
| created_at | timestamp                 | YES  |     | CURRENT_TIMESTAMP |                |
| updated_at | timestamp                 | YES  |     | NULL              |                |
| status     | enum('active','inactive') | YES  |     | NULL              |                |

###Sample

###Functions available
  1. Create table
  2. Insert a seeker
