package config

import (
	"encoding/json"
	"os"
	"path/filepath"
)

//Config Configuration Object to be used for all the resources
var Config = Configuration{}

//ConfigError error object
//ConfigError is set to a non null value if any error occurs while reading Configuration
var ConfigError error

func init() {
	path := filepath.Join(os.Getenv("GOPATH"), "src/github.com/sarathsp06/azaadi/model/config", "config.json")
	file, err := os.Open(path)
	if err != nil {
		ConfigError = err
		return
	}
	decoder := json.NewDecoder(file)
	ConfigError = decoder.Decode(&Config)
}
