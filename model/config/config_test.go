package config_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/sarathsp06/azaadi/model/config"
)

var _ = Describe("Config", func() {
	Context(`{
	  "dbs": {
		  "db1":{
			  "ip":"http://127.0.0.1",
				"port":"3306",
				"dbname":"azaadi",
				"user":"root",
				"password":"11235879"
	    }
	  },
	  "elastic":{
	    "ip":"http://127.0.0.1",
	    "port":"9200"
	  }
	}
`, func() {
		It("Should have Config.Dbname = azaadi", func() {
			Expect(Config.DBs["db1"].DBName).Should(Equal("azaadi"))
		})
		It("Should have Config.User = root", func() {
			Expect(Config.DBs["db1"].User).Should(Equal("root"))
		})
		It("Should have Config.Password = 11235879", func() {
			Expect(Config.DBs["db1"].Password).Should(Equal("11235879"))
		})
		It("Should have Config.es.port = 9200", func() {
			Expect(Config.Elastic.Port).Should(Equal("9200"))
		})
		It("There should not be any error", func() {
			Expect(ConfigError).Should(BeNil())
		})

	})
})
