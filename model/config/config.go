package config

type (
	//Elastic elastic search server configurations
	Elastic struct {
		IP   string
		Port string
	}
	//Beanstalk queu configuration
	Beanstalk struct {
		IP   string
		Port string
		Tube string
	}

	//DB configuration structure
	DB struct {
		IP       string
		Port     string
		DBName   string
		User     string
		Password string
	}

	//Configuration The Configuration structure
	//The configuration file should be compatible with this structure
	//To add any new configuration add a field in the struct define the type of it if required
	Configuration struct {
		Elastic Elastic       `json:"elastic"`
		DBs     map[string]DB `json:"dbs"`
		BS      Beanstalk     `json:"bs"`
	}
)
