package employers

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"time"

	"github.com/sarathsp06/azaadi/model"
)

//Employer is the job Employers details table
type Employer struct {
	ID          uint64 `sql:"AUTO_INCREMENT"`
	Fname       string
	Lname       string
	PhotoURL    string
	CompanyName string
	Number      string
	Email       string `sql:"unique_index"`
	Token       string
	Type        string
	CreatedAt   *time.Time `sql:"DEFAULT:current_timestamp"`
	UpdatedAt   *time.Time
}

//TableName returns the table name to be used for
//the User struct when communicating with db
func (t Employer) TableName() string {
	return "employers"
}

//Create creates contacts table in all the shards available
func Create() error {
	db, err := model.SQLConnection()
	if err != nil {
		return err
	}

	if !db.HasTable(&Employer{}) {
		err = db.CreateTable(&Employer{}).Error
		if err != nil {
			return err
		}
	} else {
		return errors.New("TABLEEXISTS:Employers")
	}

	return nil
}
func gravatar(email string) string {
	h := md5.New()
	io.WriteString(h, email)
	md5 := fmt.Sprintf("%x", h.Sum(nil))
	url := "http://www.gravatar.com/avatar/" + md5 + "?s=200"
	return url
}

//Insert inserts a Employer entry into the table
//The name is mandatory
//EmployerID is ignored even of passed
func Insert(employer *Employer) error {
	if employer.Fname == "" ||
		employer.CompanyName == "" ||
		employer.Number == "" ||
		employer.Email == "" ||
		employer.Token == "" ||
		employer.Type == "" {
		return errors.New("MANDATORY:DETAILSNOTAVAILABLE")
	}

	db, err := model.SQLConnection()
	if err != nil {
		return err
	}
	if employer.PhotoURL == "" {
		employer.PhotoURL = gravatar(employer.Email)
	}
	err = db.Create(employer).Error
	if err != nil {
		return err
	}
	return nil
}

//GetByCredentials returns an employer details
func GetByCredentials(email string, password string) (*Employer, error) {
	db, err := model.SQLConnection()
	if err != nil {
		return nil, err
	}
	var employer Employer
	err = db.
		Where("email = ?", email).
		Where("token = ?", password).
		First(&employer).Error
	if err != nil {
		return nil, err
	}
	return &employer, nil
}

var _ = Create()
