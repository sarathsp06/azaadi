###Employers
Tenants table stores a mapping of tenant id to the account sid

###Schema
| Field        | Type                | Null | Key | Default           | Extra          |
|--------------|---------------------|------|-----|-------------------|----------------|
| id           | bigint(20) unsigned | NO   | PRI | NULL              | auto_increment |
| fname        | varchar(255)        | YES  |     | NULL              |                |
| lname        | varchar(255)        | YES  |     | NULL              |                |
| photo_url    | varchar(255)        | YES  |     | NULL              |                |
| company_name | varchar(255)        | YES  |     | NULL              |                |
| number       | varchar(255)        | YES  |     | NULL              |                |
| email        | varchar(255)        | YES  |     | NULL              |                |
| token        | varchar(255)        | YES  |     | NULL              |                |
| type         | varchar(255)        | YES  |     | NULL              |                |
| created_at   | timestamp           | YES  |     | CURRENT_TIMESTAMP |                |
| updated_at   | timestamp           | YES  |     | NULL              |                |

###Sample




###Functions available
  1. Create table
  2. Insert an employer
