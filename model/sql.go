package model

import (
	"fmt"
	"log"

	"github.com/sarathsp06/azaadi/model/config"
	//initialize the mysql driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

//DB instances to be used by all the routnes like a singleton
var DB *gorm.DB

//connectionString returns the connectionString
// for the mysql server corresponding to the shardname specified
//The connection string lloks like
//   `sql.Open("mysql", "user@tcp(localhost:3306)/test?charset=utf8&parseTime=True&loc=Local"`
func connectionString() string {
	db := config.Config.DBs["db1"]
	conString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		db.User,
		db.Password,
		db.IP,
		db.Port,
		db.DBName,
	)
	return conString
}

//Connection establishes connection for sql db
//Given the shard name
//1. Checks if there is already a db connection object for the shard
//2. Creates connection with the configuration specified for the specific shard
//3. returns non null vakue for error on any failure or the connection object
func connection() (gorm.DB, error) {
	if DB != nil {
		return *DB, nil
	}
	var (
		db  gorm.DB
		err error
	)
	if db, err = gorm.Open("mysql", connectionString()); err != nil {
		log.Println("Failed creating database connection with ", connectionString(), err.Error())
		return db, err
	}
	DB = &db
	return db, nil
}

//SQLConnection establishes connection for sql db and returns the connection object
//for the shard that has tenant information
//It uses the opened connection if already available for the tenant specified
//otherwise creates a connection and returns the connection object
func SQLConnection() (db gorm.DB, err error) {
	db, err = connection()
	return
}

//TimeLayout returns the time layout string used across all for models
func TimeLayout() string {
	return "2006-01-02 15:04:05" //time layout for mysql
}

func init() {
	db, err := SQLConnection()
	if err != nil {
		log.Println(err.Error())
		return
	}

	db.DB().Ping()
	db.DB().SetMaxIdleConns(100)
	db.DB().SetMaxOpenConns(1000)
	db.LogMode(true)
	return
}
