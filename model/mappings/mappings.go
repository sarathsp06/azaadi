package mappings

import (
	"errors"

	"github.com/sarathsp06/azaadi/model"
)

//Mapping is the mappings details table
type Mapping struct {
	Sid      string `gorm:"primary_key" sql:"name:sid"`
	SeekerID uint64
	JobID    uint64
	Type     string
}

//TableName returns the table name to be used for
//the User struct when communicating with db
func (t Mapping) TableName() string {
	return "mappings"
}

//Create creates contacts table in all the shards available
func Create() error {
	db, err := model.SQLConnection()
	if err != nil {
		return err
	}
	if !db.HasTable(&Mapping{}) {
		err = db.CreateTable(&Mapping{}).Error
		if err != nil {
			return err
		}
	} else {
		return errors.New("TABLEEXISTS:mappings")
	}

	return nil
}

//GetBySID gets a mapping provided the SID
func GetBySID(SID string) (*Mapping, error) {
	db, err := model.SQLConnection()
	if err != nil {
		return nil, err
	}
	mapping := &Mapping{}
	err = db.Where(&Mapping{Sid: SID}).First(mapping).Error
	if err != nil {
		return nil, err
	}
	return mapping, nil
}

var _ = Create()
