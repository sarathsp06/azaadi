###Mappings
Mappings table contains call sid to job seeker mapping

###Schema
| Field     | Type         | Null | Key | Default | Extra |
|-----------|--------------|------|-----|---------|-------|
| sid       | varchar(255) | NO   | PRI | NULL    |       |
| seeker_id | bigint(20)   | YES  | MUL | NULL    |       |
| job_id    | bigint(20)   | YES  | MUL | NULL    |       |
| type      | varchar(32)  | YES  |     | NULL    |       ||

###Sample



###Functions available
  1. Create table
  2.Fect the maping given sid
