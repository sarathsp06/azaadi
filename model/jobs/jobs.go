package jobs

import (
	"errors"
	"time"

	"github.com/sarathsp06/azaadi/model"
)

//Job is the jobs details table
type Job struct {
	ID             uint64 `sql:"AUTO_INCREMENT"`
	Producer       uint64 `sql:"index"`
	Title          string
	Latitude       string
	Longitude      string
	Duration       string
	StartDate      *time.Time
	EndDate        *time.Time
	ExpiryDate     *time.Time
	RequestedCount uint64
	AcceptedCount  uint64
	Requested      string
	Accepted       string
	State          string `sql:"type:enum('queued','processing','completed','archived');DEFAULT:'queued'"`
	Skills         string `sql:"type:text"`
	Description    string `sql:"type:text"`
	AudioDesc      string
	CreatedAt      *time.Time `sql:"DEFAULT:current_timestamp"`
	UpdatedAt      *time.Time
}

//TableName returns the table name to be used for
//the User struct when communicating with db
func (t Job) TableName() string {
	return "jobs"
}

//Create creates contacts table in all the shards available
func Create() error {
	db, err := model.SQLConnection()
	if err != nil {
		return err
	}

	if !db.HasTable(&Job{}) {

		err = db.CreateTable(&Job{}).Error
		if err != nil {
			return err

		}
	} else {
		return errors.New("TABLEEXISTS:jobs")
	}

	return nil
}

//Insert inserts a Job entry into the table
//The name is mandatory
//JobID is ignored even of passed
func Insert(job *Job) error {
	if job.Description == "" ||
		job.Duration == "" ||
		job.EndDate == nil ||
		job.ExpiryDate == nil ||
		job.Latitude == "" ||
		job.Longitude == "" ||
		job.Producer == 0 ||
		job.StartDate == nil {
		return errors.New("MANDATORY:DETAILSNOTAVAILABLE")
	}

	db, err := model.SQLConnection()
	if err != nil {
		return err
	}
	err = db.Create(job).Error
	if err != nil {
		return err
	}
	return nil
}

//GetByID gets a job given the id
func GetByID(ID uint64) (*Job, error) {
	db, err := model.SQLConnection()
	if err != nil {
		return nil, err
	}
	var job = &Job{}
	err = db.First(job, ID).Error
	if err != nil {
		return nil, err
	}
	return job, nil
}

//GetByProducer returns all the jobs the producer has created
func GetByProducer(producerID uint64) ([]Job, error) {
	db, err := model.SQLConnection()
	if err != nil {
		return nil, err
	}
	var jobs []Job
	err = db.Where(&Job{Producer: producerID}).Find(&jobs).Error
	if err != nil {
		return nil, err
	}
	return jobs, nil
}

var _ = Create()
