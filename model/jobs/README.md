###Jobs
jobs table contains all the details about jobs available and the archived ones

###Schema
| Field           | Type                                               | Null | Key | Default           | Extra          |
|-----------------|----------------------------------------------------|------|-----|-------------------|----------------|
| id              | bigint(20) unsigned                                | NO   | PRI | NULL              | auto_increment |
| producer        | bigint(20) unsigned                                | YES  |     | NULL              |                |
| latitude        | varchar(255)                                       | YES  |     | NULL              |                |
| longitude       | varchar(255)                                       | YES  |     | NULL              |                |
| duration        | varchar(255)                                       | YES  |     | NULL              |                |
| start_date      | timestamp                                          | YES  |     | NULL              |                |
| end_date        | timestamp                                          | YES  |     | NULL              |                |
| expiry_date     | timestamp                                          | YES  |     | NULL              |                |
| requested_count | bigint(20) unsigned                                | YES  |     | NULL              |                |
| accepted_count  | bigint(20) unsigned                                | YES  |     | NULL              |                |
| requested       | varchar(255)                                       | YES  |     | NULL              |                |
| accepted        | varchar(255)                                       | YES  |     | NULL              |                |
| state           | enum('queued','processing','completed','archived') | YES  |     | queued            |                |
| description     | text                                               | YES  |     | NULL              |                |
| audio_desc      | varchar(255)                                       | YES  |     | NULL              |                |
| created_at      | timestamp                                          | YES  |     | CURRENT_TIMESTAMP |                |
| updated_at      | timestamp                                          | YES  |     | NULL              |                |

###Sample



###Functions available
  1. Create table
  2. Insert a seeker
