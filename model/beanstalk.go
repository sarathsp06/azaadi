package model

import (
	"encoding/json"
	"fmt"

	"github.com/nutrun/lentil"
	"github.com/sarathsp06/azaadi/model/config"
)

var bsConnection *Client

//Client Beanstalkd client
type Client struct {
	Conn *lentil.Beanstalkd
}

//NewBSClient Creates a Beanstalkd bsConnection and returns
//If it already exists the existing one is send otherwise new one is created and send
func NewBSClient() (*Client, error) {
	if bsConnection != nil {
		return bsConnection, nil
	}
	esConfig := config.Config.BS
	conn, err := lentil.Dial(esConfig.IP + ":" + esConfig.Port)
	if err != nil {
		return nil, err
	}
	conn.Use(esConfig.Tube)
	bsConnection = &Client{Conn: conn}
	return bsConnection, nil
}

//List prints the lists
func (c *Client) List() {
	tubes, _ := c.Conn.ListTubes()
	for _, tube := range tubes {
		fmt.Println(tube)
	}
}

//Put puts an object and returns the jobid
func (c *Client) Put(obj interface{}) (uint64, error) {
	job, err := json.Marshal(obj)
	var jobID uint64
	jobID, err = c.Conn.Put(0, 0, 60, job)
	if err != nil {
		return 0, err
	}
	return jobID, err
}
