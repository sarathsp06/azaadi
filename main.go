package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/sarathsp06/azaadi/handlers"
)

func main() {
	router := gin.New()
	CORS(router)
	addHandlers(router)
	router.Run(":8080")
}

func addHandlers(router *gin.Engine) {
	path := filepath.Join(os.Getenv("GOPATH"), "src/github.com/sarathsp06/azaadi/assets")
	router.StaticFS("/assets", http.Dir(path))

	router.POST("/job", handlers.JobPostHandler)
	router.POST("/seeker", handlers.SeekerPostHandler)
	router.POST("/employer", handlers.EmployerPostHandler)
	router.POST("/employer/login", handlers.EmployerLoginHandler)
	router.POST("/statuscallback", handlers.CallBackHandler)

	router.GET("/passthrough", handlers.PassthroughHandler)
	router.GET("/job/:producer", handlers.JobGetHandler)
	router.GET("/dynamicurl", handlers.DynamicURLHandler)
	router.GET("/seeker/:id", handlers.SeekerGetHandler)
	// router.GET("/employer", handlers.EmployerGetHandler)
}

//CORS cors setter
func CORS(router *gin.Engine) {
	router.Use(func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			fmt.Println("OPTIONS")
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	})
}
